package com.ninjahoahong.myapplication.engine.board.test1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class BoardView extends View {

    private int originalWidth;
    private int originalHeight;
    private int aspectRatioWidth;
    private int aspectRatioHeight;
    private int finalWidth = originalWidth;
    private int finalHeight = originalHeight;
    private int widthWithoutBorder;
    private int heightWithoutBorder;
    private static final int BORDER_THICKNESS  = 2;
    private Paint boardPaint;
    private Board board = null;
    private BoardGame boardGame;

    public BoardView(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public BoardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public BoardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    public BoardView(Context context,
                     @Nullable AttributeSet attrs,
                     int defStyleAttr,
                     int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    public void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        boardPaint = new Paint();
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public void setAspectRatio(int aspectRatioWidth, int aspectRatioHeight) {
        this.aspectRatioWidth = aspectRatioWidth;
        this.aspectRatioHeight = aspectRatioHeight;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        originalHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        originalWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        int calculatedHeight = originalWidth * aspectRatioHeight / aspectRatioWidth;


        if (calculatedHeight > originalHeight)
        {
            finalWidth = originalHeight * aspectRatioWidth / aspectRatioHeight;
            finalHeight = originalHeight;
        }
        else
        {
            finalWidth = originalWidth;
            finalHeight = calculatedHeight;
        }

        widthWithoutBorder = (finalWidth - BORDER_THICKNESS) / board.getWidth();
        heightWithoutBorder = (finalHeight - BORDER_THICKNESS) / board.getHeight();


        super.onMeasure(
            MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY),
            MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawBoard(canvas);
    }

    private void drawBoard(Canvas canvas) {
        if (board == null) {
            throw new IllegalStateException("Board is null, need to set the board.");
        }
        for (int i = 0; i <= board.getWidth(); ++i) {
            float left = widthWithoutBorder * i;
            float right = left + BORDER_THICKNESS;
            float top = 0;
            float bottom = finalHeight;
            canvas.drawRect(left, top, right, bottom, boardPaint);
        }

        for (int j = 0; j <= board.getHeight(); ++j) {
            float left = 0;
            float right = finalWidth;
            float top = heightWithoutBorder * j;
            float bottom = top + BORDER_THICKNESS;
            canvas.drawRect(left, top, right, bottom, boardPaint);
        }
    }
}
