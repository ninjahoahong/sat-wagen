package com.ninjahoahong.myapplication.engine.triangle.test2;

import android.opengl.GLSurfaceView;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class TriangleTest2Activity extends AppCompatActivity {

    GLSurfaceView glView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

//        BoardView boardView = findViewById(R.id.boardView);
//        Board board = new Board(4, 3, new ArrayList<Piece>());
//        boardView.setAspectRatio(4, 3);
//        boardView.setBoard(board);


        glView = new MyGLSurfaceView(this);
        setContentView(glView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The following call pauses the rendering thread.
        // If your OpenGL application is memory intensive,
        // you should consider de-allocating objects that
        // consume significant memory here.
        glView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The following call resumes a paused rendering thread.
        // If you de-allocated graphic objects for onPause()
        // this is a good place to re-allocate them.
        glView.onPause();
    }
}
