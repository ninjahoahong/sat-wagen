package com.ninjahoahong.myapplication.engine.board.test1;

import android.os.Bundle;

import com.ninjahoahong.myapplication.R;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class BoardTest1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BoardView boardView = findViewById(R.id.boardView);
        Board board = new Board(4, 3, new ArrayList<Piece>());
        boardView.setAspectRatio(4, 3);
        boardView.setBoard(board);
    }
}
