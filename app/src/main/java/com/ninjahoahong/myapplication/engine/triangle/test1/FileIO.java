package com.ninjahoahong.myapplication.engine.triangle.test1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//TODO ALLOC
public interface FileIO {
    public InputStream readAsset(String fileName) throws IOException;

    public InputStream readFile(String fileName) throws IOException;

    public OutputStream writeFile(String fileName) throws IOException;
}