package com.ninjahoahong.myapplication.engine.board.test1;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private int height;
    private int width;
    private List<Piece> pieces = new ArrayList<>();

    public Board(){}

    public Board(int width,
                 int height,
                 List<Piece> pieces) {
        this.height = height;
        this.width = width;
        this.pieces = pieces;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setPieces(List<Piece> pieces) {
        this.pieces = pieces;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public void clear() {
        pieces.clear();
    }

    public void move(Piece piece, Position pivot){
        piece.setPivot(pivot);
    }
}
