package com.ninjahoahong.myapplication.engine.board.test1;

import com.ninjahoahong.myapplication.engine.animation.Sprite;

import java.util.List;

public class Piece {
    private long id;
    private Sprite sprite;
    private Position pivot; // Let's set pivot as top left of the piece
    private List<Position> positions; //relative position to the pivot

    public Piece() {}

    public Piece(Long id,
                 Sprite sprite,
                 List<Position> positions, Position pivot) {
        this.id = id;
        this.sprite = sprite;
        this.positions = positions;
        this.pivot = pivot;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public Position getPivot() {
        return pivot;
    }

    public void setPivot(Position pivot) {
        this.pivot = pivot;
    }
}
