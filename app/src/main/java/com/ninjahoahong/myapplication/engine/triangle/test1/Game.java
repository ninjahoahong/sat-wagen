package com.ninjahoahong.myapplication.engine.triangle.test1;

//TODO ALLOC
public interface Game {

    public Graphics getGraphics();

    public void setScreen(Screen screen);

    public Screen getCurrentScreen();

    public Input getInput();

    public Audio getAudio();

    public Screen getStartScreen();

    public FileIO getFileIO();
}