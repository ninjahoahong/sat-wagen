package com.ninjahoahong.myapplication.engine.map;

public class Map {
    public static int[][] Map2Ds;

    public static void createMap2Ds() {
        Map2Ds = new int[4][];
        int[] map0 = {0, 0, 1, 1, 2, 2, 3, 3, 0, 0, 1, 1, 2, 2, 3, 2, 2, 3, 3};
        int[] map1 = {0, 0, 1, 1, 2, 2, 3, 3, 0, 0, 1, 1, 2, 2, 3, 2, 2, 3, 3, 0, 0, 1, 1, 2, 3, 3, 0, 0, 1, 1};
        int[] map2 = {2, 2, 2, 2, 2, 2, 2, 2,
            1, 1, 3, 3, 1, 1, 1, 1,
            3, 2, 5, 5, 2, 5, 5, 5,
            4, 4, 4
        };
        Map2Ds[0] = map0;
        Map2Ds[1] = map1;
        Map2Ds[2] = map2;
    }
}
