package com.ninjahoahong.myapplication.engine.myengine;

import android.os.Bundle;

import com.ninjahoahong.myapplication.R;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Will be the main board game engine when the tests all done.
 */
public class MyEngineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
