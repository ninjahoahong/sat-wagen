package com.ninjahoahong.myapplication.engine.board.test1;

import android.view.View;

public interface BoardGame {
    public TouchHandler getTouchHandler();
    public void setView(View currentView);
    public void getCurrentView();
}
